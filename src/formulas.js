/**
 * @constant {Object<string,number>} priority - maps the relative priorities of Operations.
 */
const priority = {

	/**
	 * NOTE: the priority of '(' must be lower than ALL other operations
	 * for the Formula parsing algorithm to work.
	 * Priorities are not included in tokens since they're only needed during parsing.
	 */
	'isNaN':7, 'ceil':7, 'floor':7, 'min':7, 'max':7, '>':3, '<':3, '>=':3, '<=':3, '==':3, '!=':3,'!':3, '||':2, '&&':2,
	'=':1,'+':4,'-':4,'/':5,'*':5, '%':5, '(':0

};

/**
 * @constant {RegExp} hasOpRegEx - tests if a string contains an operation substring..
 */
const hasOpRegEx = /isnan|ceil|floor|min|max|\>|\<|\>=|\<=|==|\!=|\!|=|\+|\-|\/|\*|\%|\|\||\&\&/i;

/**
 *
 * Tests if a string contains a formula operation. Astring without an operation
 * should probably not be parsed as a formula.
 * @function
 * @param {string} str - the string to be tested.
 * @returns {bool} true iff the string contains an operation.
 */
exports.containsOp = ( str ) => {
	return hasOpRegEx.test( str );
}

// ERRORS
const ERR_NONE = 0
const ERR_PARSE = 1;
const ERR_VAR_UNDEFINED = 2;

exports.ERR_NONE = ERR_NONE;
exports.ERR_PARSE = ERR_PARSE;
exports.ERR_VAR_UNDEFINED = ERR_VAR_UNDEFINED;


// TOKEN TYPES
const CONST = 1;
const VAR = 2;
const OP = 3;
const OPEN = 4; // parens
const CLOSE = 5;
const ERR = 0;

/**
 * Represents a Token of a Formula.
 */
class Token {

	/**
	 * @property {*} value - Token value.
	 */
	get value() { return this.val; }

	/**
	 *
	 * @param {*} val - raw token value.
	 * @param {Number} type - Token type.
	 */
	constructor( val, type ) {

		//console.log('creating token: ' + val );

		this.val = val;
		this.type = type;
	}

	/**
	 * Evaluates the token value using properties defined on the source object.
	 * @param {Object} src - object whose properties can be queried for values.
	 * @returns {*}
	 */
	eval( src ) {

		if ( this.type === CONST ) return this.val;
		let path = this.val.split('.');
		let len = path.length;

		let obj = src;
		if ( len === 1 ) {
			// possible to return a constant value.
			// TODO: introduce quote marks and/or force a single '.' on path.
			return src[path[0]];
		}

		for (let i = 0; i < len; i++) {

			if (!(obj instanceof Object)) return undefined;
			obj = obj[ path[i] ];

		}

		return obj;

	}

}

/**
 * Token for an operation within a Formula.
 */
class Op extends Token {

	/**
	 *
	 * @param {string} opStr - raw token input.
	 * @param {function} opFunc - function which computes the operation.
	 * @param {Number} argCount - number of arguments of the operation.
	 */
	constructor( opStr, opFunc, argCount=2 ) {

		super( opStr, OP );
		this.argCount = argCount;
		this.func = opFunc;

	}

}

/**
 * @constant {Object<string,Op>} opList - Valid operations within Formulas.
 */
const opList = {

	'+':new Op( '+', (a,b)=>a+b ),
	'-':new Op( '-', (a,b)=>a-b),
	'/':new Op( '/', (a,b)=>a/b),
	'*':new Op( '*', (a,b)=>a*b),
	'%':new Op( '%', (a,b)=>a%b),
	'!':new Op( '!', a=>!a, 1 ),
	'>':new Op( '>', (a,b)=>a>b),
	'<':new Op( '<', (a,b)=>a<b),
	'>=':new Op( '>=', (a,b)=>a>=b),
	'<=':new Op( '<=', (a,b)=>a<=b),
	'==':new Op( '==', (a,b)=> a===b),
	'!=':new Op( '!=', (a,b)=>a!==b),
	'||':new Op( '||', (a,b)=>a||b),
	'&&':new Op( '&&', (a,b)=>a&&b),
	'=':new Op( '=', null ),
	'ceil':new Op( 'ceil',(a)=>Math.ceil(a), 1 ),
	'floor':new Op( 'floor',(a)=>Math.floor(a), 1 ),
	'min':new Op( 'min',(a,b)=>Math.min(a,b) ),
	'max':new Op( 'max',(a,b)=>Math.max(a,b) ),
	'isNaN':new Op( 'isNaN', (a)=>isNaN(a), 1),
	'(':new Token( '(', OPEN ),
	')':new Token(')', CLOSE)

};

/**
 * Represents a Formula which can be computed using properties/values
 * from a source object.
 */
class Formula {

	/**
	 * @static
	 * Use the module's default FormulaParser to parse a string
	 * into a valid formula.
	 * @param {string} str - raw formula text.
	 * @returns {Formula|boolean}
	 */
	static TryParse(str) {

		if ( !str ) return false;
		let f = DefaultParser.parse( str );
		f._error = DefaultParser.error;

		return f;

	}

	/**
	 * @property {*} error
	 */
	get error() { return this._error; }
	set error(v) { this._error = v; }

	/**
	 * @returns {string} raw formula string.
	 */
	toString() { return this._str; }

	/**
	 *
	 * @returns {Token[]} Tokenized representation of the formula.
	 */
	getQueue() { return this.queue; }

	constructor( str, queue=null) {

		this._str = str;
		this.queue = queue || DefaultParser.parse(str).queue;

		/*for( let i = this.queue.length-1; i >= 0; i-- ) {
			console.log('stack: ' + this.queue[i].value);
		}*/

	}

	/**
	 * @returns {string}
	 */
	toJSON() { return this._str; }

	/**
	 * @returns {bool} true if the last computation of this Formula
	 * resulted in an error.
	 */
	hasError() { return this._error }

	/**
	 *
	 * @param {Object} vars - variable inputs to the formula evaluator.
	 * @param {?Object} [target=null] - target for any assignment operations
	 * within the formula.
	 * @returns {*} The computed value of the Formula based on the input variables.
	 */
	eval( vars, target=null ) {

		this.error = ERR_NONE;
		this._target = target;

		// next item from stack.
		let qIndex = 0, len = this.queue.length;

		let vals = [];	// values read from stack.

		let cur;

		while ( qIndex < len ) {

			cur = this.queue[qIndex++];

			if ( cur.type === OP ){

				if ( vals.length < cur.argCount ) {
					console.error( `OP ${cur.value} requires ${cur.argCount} args: ${this._str}` );
					break;
				}
				if ( cur.value === '=') {

					vals.push( this.assign( vars, vals, target ) );

				} else vals.push( this.doOp( cur, vars, vals) );

			} else vals.push( cur );

		}

		if ( vals.length === 0 ) console.error( 'ERR: no output: ' + this._str );

		return vals.pop();

	}

	/**
	 * Perform an assignment to the current target object.
	 * @param {Object} source - source for getting values from a variable path string.
	 * @param {Token[]} tokens - Stack of Formula tokens.
	 * @param {?Object} [target=null] - Assignment target.
	 * @returns {*} the value assigned.
	 */
	assign( source, tokens, target ) {

		let y = tokens.pop();
		let x = tokens.pop();

		if ( x instanceof Token ) x = x.value;	// string value.
		if ( y instanceof Token ) y = y.eval(source);

		if ( x === undefined ) this.error = ERR_VAR_UNDEFINED;

		return target ? target[x] = y : y;

	}

	/**
	 *
	 * @param {Op} op - operation token.
	 * @param {Object} vars
	 * @param {Token[]} tokens - formula stack.
	 * @returns {*} - operation result.
	 */
	doOp( op, vars, tokens ) {

		let count = op.argCount;
		let params = tokens.splice( -count, count );

		let param;
		for( let i = 0; i < count; i++ ) {

			param = params[i];

			if ( param instanceof Token ) {
				//console.log('param var: ' + param.val);
				if ( (params[i] = param.eval(vars)) === undefined ){
					params[i] = param.value;
				}

			}
			//console.log( 'param: ' + params[i]);

		}

		return op.func.apply( this, params );

	}

}

class FormulaParser {

	constructor() {}

	/**
	 * Error produced by the last parse attempt.
	 * @property {*} error
	 */
	get error() { return this._error; }

	/**
	 * True if the last parse attempt produced an error.
	 * @returns {boolean}
	 */
	hasError() { return this._error != null; }

	/**
	 *
	 * @param {string} str
	 * @returns {Formula}
	 */
	parse( str ) {

		this._error = false;

		this.str = str;
		let numChars = this.charCount = str.length;

		this.index = 0;
		let opStack = [];
		let resStack = [];

		var token, top = null;
		var curPriority;

		while ( this.index < numChars ) {

			token = this.readToken();
			if ( token.type === OP) {

				curPriority = priority[token.value];
				while( opStack.length > 0 ) {

					top = opStack.pop();
					if ( top === null ) {
						console.error( 'unexpected null op: ' + this.str );
						break;
					} else if ( priority[top.value] < curPriority) {
						opStack.push( top );	// todo: slightly inefficient.
						break;
					}

					resStack.push( top );	// high-priority op to output.

				}
				opStack.push( token );

			} else if ( token.type === OPEN ) {

				opStack.push( token );

			} else if ( token.type === CLOSE ) {

				while ( opStack.length > 0 ) {

					// pop to open paren.
					top = opStack.pop();

					if ( top.type === OPEN ) break;
					resStack.push( top );

				}
				if ( top === null || top.type !== OPEN ) console.error('Missing close paren: ' + top.value );

			} else {
				resStack.push( token );
			}

		}

		while ( opStack.length > 0 ) resStack.push( opStack.pop() );

		return new Formula( this.str, resStack );

	}

	/**
	 * Read the next token from the source string.
	 * @returns {Token}
	 */
	readToken() {

		let c = this.str[ this.index ];

		while ( c === ' ' || c === '\t' || c === ',') c = this.str[ ++this.index ];

		let charCode = c.charCodeAt(0);
		if ( this.isChar(charCode) ) return this.readVar();

		let op = this.matchOp();
		if ( op !== null ) return op;

		if ( this.isNumeric( charCode ) ) {

			return this.readNumber();

		}

		//console.log( `INVALID CHAR AT  ${(this.index)}: ${c}` );
		this.index++

		this._error = true;
		return new Token( 0, ERR );

	}

	/**
	 * Attempt to match an operator at the current input position.
	 * TODO: Use a tree-structure for efficiency.
	 * Note that the longest possible operator must be matched.
	 * e.g. == instead of =
	 * @returns {?Op}
	 */
	matchOp() {

		let start = this.index;

		var lastOp = null;
		var lastLen = 0;

		for( let opStr in opList ) {

			var op = opList[opStr];
			var opLen = opStr.length;

			// longer op already found.
			if ( opLen <= lastLen ) continue;
			// not enough chars.
			if ( start+opLen > this.charCount) continue;

			if ( opStr === this.str.substr(start, opLen) ) {
				lastOp = op;
				lastLen = opLen;
			}

		}

		if ( lastOp ) this.index = start + lastLen;
		return lastOp;

	}

	/**
	 * Read a number constant from input.
	 * The current char in input must be a numeric char.
	 * @returns {Token} CONST token.
	 */
	readNumber() {

		let start = this.index;
		let ind = start;

		while ( this.isNumeric( this.str.charCodeAt(++ind ) ) );

		this.index = ind;
		return new Token( Number( this.str.slice(start, ind )), CONST );

	}

	/**
	 * read a variable path when the current value in input is a character value.
	 * @returns {Token} VAR token.
	 */
	readVar() {

		let start = this.index;
		let ind = start;
		while ( this.isCharOrVar( this.str.charCodeAt(++ind ) ) );

		this.index = ind;

		// check for string operator.
		let text = this.str.slice( start, ind );

		if ( opList.hasOwnProperty(text)) return opList[text];

		return new Token( text, VAR );

	}

	/**
	 *
	 * @param {number} n
	 * @returns {boolean}
	 */
	isDigit( n ) { return n >= 48 && n <= 57; }
	/**
	 *
	 * @param {number} n
	 * @returns {boolean}
	 */
	isNumeric( n ) { return (n >= 48 && n <= 57) || n === 43 || n === 45 || n === 46 }
	/**
	 *
	 * @param {number} n
	 * @returns {boolean}
	 */
	isChar( n ) { return (n >= 65 && n <= 90) || (n >= 97 && n <= 122) }

	/**
	 *
	 * @param {number} n
	 * @returns {boolean}
	 */
	isCharOrVar( n ) { return (n >= 65 && n <= 90) || (n >= 97 && n <= 122) || (n >= 48 && n <= 57) || n === 46 || n === 95; }

}

var DefaultParser = new FormulaParser();

exports.DefaultParser = DefaultParser;
exports.FormulaParser = FormulaParser;
exports.Formula = Formula;