# Formulic
Define formulas and computations in JSON for use in javascript.

A formulic json file defines named WorkDef objects ( Work Definitions ) which apply
formulas to input objects provided by javascript.

Sample JSON:

```
{
	"purchaseCost":{
		"props":["cart"],
		"vars":{

		},
		"items":[

			{
				"foreach":{
					"var":"cart.items",
					"set":"itemInfo",
					"name":"itemCost"
				}
			}

		]
	},
	"itemCost":{
		"props":["itemInfo"],
		"vars":{

			"price":"itemInfo.cost"
		}
	}

}
```

Sample Code:
```
let runner = new Runner( workDefs );
let workItem = runner.run( 'purchaseCost', { cart:myCart }, { tax:0.07 });

```